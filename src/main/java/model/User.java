/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Lenovo
 */
public class User {
    private int id;
    private String name;
    private String tel;
    private String password;

    public User(int id, String name, String tel, String password) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.password = password;
    }

    public User(String name, String tel, String password) {
        this(-1,name,tel,password);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", password=" + password + '}';
    }   
    
}
